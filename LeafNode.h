//Students: Kelvin Lu, Dinesh Jayasankar

#ifndef LeafNodeH
#define LeafNodeH

#include "BTreeNode.h"

class LeafNode:public BTreeNode
{ 
  int maxValue;
  int *values;
public:
  LeafNode(int LSize, InternalNode *p, BTreeNode *left,
    BTreeNode *right);
  int getMinimum() const;
  LeafNode* insert(int value); // returns pointer to new Leaf if splits
  // else NULL
  void print(Queue <BTreeNode*> &queue);
  LeafNode* splitAndInsert(const int&);
  bool isFull();
private:
  int getPosition(const int& value);
  int getPosition2(const int& value);

}; //LeafNode class

#endif
