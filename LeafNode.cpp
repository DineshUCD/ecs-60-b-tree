//Students: Kelvin Lu, Dinesh Jayasankar

#include <iostream>
#include "LeafNode.h"
#include "InternalNode.h"
#include "QueueAr.h"

using namespace std;


LeafNode::LeafNode(int LSize, InternalNode *p,
  BTreeNode *left, BTreeNode *right) : BTreeNode(LSize, p, left, right), maxValue(0)
{
  values = new int[LSize];
}  // LeafNode()


int LeafNode::getMinimum()const
{
  if(count > 0)  // should always be the case
    return values[0];
  else
    return 0;
} // LeafNode::getMinimum()


LeafNode* LeafNode::insert(int value)
{
  if (getCount() < leafSize) 
  {
    //Insert in current leaf
    values[getPosition(value)] = value;
    count++;
    return NULL;
  }
  else if (count == leafSize)
  {

    if (getLeftSibling() != NULL && this->getLeftSibling()->getCount() < leafSize)
    {
      //Pawn minimum to left sibling
      if (getMinimum() > value) 
        getLeftSibling()->insert(value); 
      else
      { 
      getLeftSibling()->insert(getMinimum());
      for (int index = 1; index < count; index++)
        values[index - 1] = values[index];
      count--;
      insert(value);
      }
      return NULL;
    }
    else if (getRightSibling() != NULL && this->getRightSibling()->getCount() < leafSize)
    {
      //Pawn maximum to right sibling
      if (values[count - 1] < value)
        getRightSibling()->insert(value);
      else
      {
        getRightSibling()->insert(values[leafSize - 1]);
        count--;
        insert(value);
      }
      return NULL;
    }
    else
    {
      //Split
      return splitAndInsert(value);
    } 
  }  //else if(count == leafSize)

  //Optional: Avoids warnings.
  return NULL;
}  // LeafNode::insert()


void LeafNode::print(Queue <BTreeNode*> &queue)
{
  cout << "Leaf: ";
  for (int i = 0; i < count; i++)
    cout << values[i] << ' ';
  cout << endl;
} // LeafNode::print()


int LeafNode::getPosition(const int& value)
{
  int pos = count - 1;

  if (count == 0) 
    return 0;

  if (values[count - 1] < value) 
  {
    maxValue = value;
    return pos + 1;
  }

  for (pos = count - 1; (pos >= 0) && (values[pos] > value); pos--)
  { 
      values[pos + 1] = values[pos];
  }
  maxValue = values[count];

  return (pos + 1);
} //LeafNode::getPosition()


int LeafNode::getPosition2(const int& value)
{
  int pos = 0;

  if (count == 0) 
    return 0;

  if (values[1] > value) 
    return pos;

  for (pos = 1; (pos < count) && (values[pos] < value); pos++)
    values[pos] = values[pos + 1];
      
  maxValue = values[count];

  return pos;
} //LeafNode::getPosition2()


LeafNode * LeafNode::splitAndInsert(const int& value)
{
  //Create a new leaf and move half our members to it
  //If value should be placed in this node

  int split[leafSize + 1];
  for (int reset = 0; reset < leafSize + 1; reset++)
    split[reset] = 0;

  for (register int copy = 0; copy < count; copy++)
    split[copy] = values[copy];

  int findPos = 0;
  for (findPos = leafSize; findPos >= 0; findPos--)
  {
    if (split[findPos] == 0)
      ;
    else if (value < split[findPos])
     split[findPos + 1] = split[findPos];
    else
      break;
  }

  split[findPos + 1] = value; 

  int midSize = ((leafSize + 1) / 2);

  this->count = 0;

  for (register int replace = 0; replace < midSize; replace++)
  {
    values[replace] = (split[replace]);
    count++;
  }

  BTreeNode * right = NULL;

  right = ((BTreeNode *)this)->getRightSibling();

  LeafNode * temp = new LeafNode(leafSize, this->parent, ((BTreeNode*)(this)), right); 


  if (getRightSibling() != NULL)
  {
    this->getRightSibling()->setLeftSibling(temp);
  }
 
  this->setRightSibling(temp);

  for (register int place = (midSize); place < leafSize + 1; place++)
  {
    temp->insert(split[place]);
  } //for() 
  
  maxValue = values[midSize - 1];
  return temp;
}

bool LeafNode::isFull()
{
  return count == leafSize;
}
