//Students: Kelvin Lu, Dinesh Jayasankar

#include <iostream>
#include "BTree.h"
#include "BTreeNode.h"
#include "LeafNode.h"
#include "InternalNode.h"
using namespace std;

typedef LeafNode * LEAF;
typedef InternalNode * INTERNAL;

BTree::BTree(int ISize, int LSize):internalSize(ISize), leafSize(LSize)
{
  root = new LeafNode(LSize, NULL, NULL, NULL);
} // BTree::BTree()


void BTree::insert(const int value)
{

  if(BTreeNode* split = root->insert(value))
  {
    //If the root has split, make a new root (InternalNode)
    INTERNAL new_root = new InternalNode(internalSize, leafSize, NULL, NULL, NULL);
    //And make the old root and its split sibling as the new root's
    //children
    new_root->insert(root, split);
    //Remember to set root as new_root
    root = new_root;
  }

} // BTree::insert()


void BTree::print()
{
  BTreeNode *BTreeNodePtr;
  Queue<BTreeNode*> queue(1000);

  queue.enqueue(root);
  while(!queue.isEmpty())
  {
    BTreeNodePtr = queue.dequeue();
    BTreeNodePtr->print(queue);
  } // while
} // BTree::print()
