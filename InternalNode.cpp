//Students: Kelvin Lu, Dinesh Jayasankar

#include <iostream>
#include "InternalNode.h"

using namespace std;

InternalNode::InternalNode(int ISize, int LSize,
  InternalNode *p, BTreeNode *left, BTreeNode *right) :
  BTreeNode(LSize, p, left, right), internalSize(ISize)
{
  keys = new int[internalSize]; // keys[i] is the minimum of children[i]
  children = new BTreeNode* [ISize];
} // InternalNode::InternalNode()

int InternalNode::getMinimum()const
{
  if(count > 0)   // should always be the case
    return children[0]->getMinimum();
  else
    return 0;
} // InternalNode::getMinimum()


InternalNode* InternalNode::insert(int value)
{
  //Find proper index
  int index;

  for (index = 0; (index < count - 1) && (keys[index] < value); index++);
    //would return the new split node

  if (keys[index] > value && index != 0)
    index--;

  BTreeNode * check = children[index]->insert(value);
  //LeafNode object checks if self-insertion or pawning is possible
  
  if (check != NULL)
  {
    if (this->getCount() == internalSize)
    {
  
      if (this->getLeftSibling() != NULL && this->getLeftSibling()->getCount() < internalSize)
      {
        //Minimum of right sub tree should have index 0
        this->updateKeys();
        ((InternalNode *)getLeftSibling())->insert(this->children[0]); 
        ((InternalNode *)getLeftSibling())->updateKeys();
        this->updateKeys();
        for (register int move = 1; move < count; move++)
          children[move - 1] = children[move];
        count--;
        this->updateKeys();
        this->insert(check);
        this->parent->updateKeys();
       //half full
      }
      else if (getRightSibling() != NULL && this->getRightSibling()->getCount() < internalSize)
      {
        //cout << "InternalNode::insert() call, pawning to right" << endl;
        this->updateKeys();
        if (children[internalSize - 1]->getMinimum() < check->getMinimum())
        {
          ((InternalNode *)getRightSibling())->insert(check);
          check->setParent( (InternalNode *)getRightSibling() );
          ((InternalNode *)getRightSibling())->updateKeys();
        }
        else
        {
          children[internalSize - 1]->setParent( (InternalNode *)getRightSibling() );
          ((InternalNode *)getRightSibling())->insert(children[count- 1]);
          ((InternalNode *)getRightSibling())->updateKeys();
          count--;
          this->updateKeys();
          this->insert(check);
        }
      }
      else
      {
        //Create a new internal node if needed
        InternalNode * temp_ptr = splitAndInsert(check);
        temp_ptr->updateKeys();
        this->updateKeys();
        temp_ptr->setParent(this->parent);
        if (parent != NULL)
          this->parent->updateKeys();
        return temp_ptr;
      } 
    }
    else if (this->getCount() < internalSize)
    {
      //array index is 1 less than count
      int getPos = getPosition(check->getMinimum());
      this->children[getPos] = check;
      this->keys[getPos] = check->getMinimum();
      check->setParent(this);
      count++;
      this->updateKeys();
      return NULL;
    }
  }

  this->updateKeys();
  return NULL;
} // InternalNode::insert()

void InternalNode::insert(BTreeNode *oldRoot, BTreeNode *node2)
{ // Node must be the root, and node1
  this->children[0] = oldRoot;
  oldRoot->setParent(this);
  this->keys[0] = (oldRoot->getMinimum());
  count++;

  this->children[1] = node2;
  node2->setParent(this);
  this->keys[1] = (node2->getMinimum());
  count++;
} // InternalNode::insert()

void InternalNode::insert(BTreeNode *newNode) // from a sibling
{
  //get minimum of new node and find a place for it in children
  newNode->setParent(this);
  int pos = getPosition(newNode->getMinimum());
  this->children[pos] = newNode;
  count++;

  if (this->parent != NULL)
  {
    this->parent->updateKeys();
    //cout << "SKIPPED" << endl;
  }
  // students may write this
} // InternalNode::insert()

void InternalNode::print(Queue <BTreeNode*> &queue)
{
  int i;

  cout << "Internal: ";
  for (i = 0; i < count; i++)
    cout << keys[i] << ' ';
  cout << endl;

  for(i = 0; i < count; i++)
    queue.enqueue(children[i]);

} // InternalNode::print()

int InternalNode::getPosition(const int & key)
{
  int pos;

  if (count == 0) return 0;
  

    if (keys[count - 1] < key) return count;

  for (pos = count - 1; (pos >= 0) && (keys[pos] > key); pos--)
  {
      keys[pos + 1] = keys[pos];
      children[pos + 1] = children[pos];
  }
  return pos + 1;
}

void InternalNode::updateKeys()
{
  for (register int index = 0; index < count; index++)
    keys[index] = children[index]->getMinimum();
}

InternalNode * InternalNode::splitAndInsert(BTreeNode * leftOver)
{


  BTreeNode **split;
  split = new BTreeNode* [internalSize + 1];

  for (int reset = 0; reset < internalSize + 1; reset++)
    split[reset] = NULL;

  for (register int copy = 0; copy < count; copy++)
  {
    split[copy] = children[copy];
  }
  int findPos = 0;
  for (findPos = internalSize; findPos >= 0; findPos--)
  {
    if (split[findPos] != NULL && leftOver->getMinimum() < split[findPos]->getMinimum())
     split[findPos + 1] = split[findPos];
    else if (split[findPos] != NULL)
      break;
  }

  split[findPos + 1] = leftOver;

  int midSize = ((internalSize + 1) / 2);
  this->count = 0;
  for (register int replace = 0; replace < midSize; replace++)
  {
    (children[replace]) = (split[replace]);
    count++;
  }

  BTreeNode * right = ((BTreeNode *)this)->getRightSibling();
  InternalNode * temp = new InternalNode(internalSize, leafSize, this->parent, ((BTreeNode*)(this)), right);

  if (getRightSibling() != NULL)
  {
    this->getRightSibling()->setLeftSibling(temp);
  }

  this->setRightSibling(temp);

  for (register int place = (midSize); place < internalSize + 1; place++)
  {
    temp->insert(split[place]);
  } //for() 
  temp->updateKeys();
  this->updateKeys();

  delete [] split;
  return temp;
}